Member: Ayooluwakunmi Jeje
    Week 1:
    
    - Set up the group git repository.
    - Helped with the initial implementation of examplePlayer.
    - Provided insight into a working method of beating SimplePlayer.
    - Helped to debug final code.
    - Thought up an addition to make AI better
    - Added the helper function updateScoreBoard() to improve chances of
      winning
    
    Week 2:
    
    - Provided an initial working improved version of the AI. 
    - Improved version won 70% of the time against ConstantTimePlayer
    - Helped debug final version of code
    
Member: Manuel Arene
    Week 1
    
    - Helped debug initial implementation of examplePlayer
    - Handled most of the interaction with the team repository
    - Coded and implemented final working version of code in part one.
    
    Week 2:
    
    - Helped debug initial working version of AI.
    - Improved on the additions added to AI
    - Implemented helper functions compromisable, update and updateBoard
    - Provided insight into a working method to beat ConstantPlayer
    - addition enabled winning 80 - 90% of the time
    - Produced final version of working code.
    
    

Improvements:

    - Basic strategy is to work from the outside, inwards.
    - AI considers corner squares to be the best. 
    - We created a scoreboard that held the "score" of each square in the
      the board.
    - The higher the score, the "better" the square.
    - A score of at least 4 indicates a square that we occupy, which can't
      be captured by the enemy.
    - Further more, given a square, c, we considered the 8 immediate 
      adjacent neighbors to c. If five successive neighbors had scores atleast
      4, we made the cell, c, have a score of 5. In other words, it became a new
      corner.
    - The idea was that a decision tree that looks about 3 or so steps ahead
      might consider a corner square to not be as valuable in 3 steps. This is
      because we believe that the worth of a corner square is seen towards the
      end of the game, allowing massive takeovers. Thus, looking 3 steps ahead
      might cause one to overlook a corner square, whereas in 5, 6, 10 steps, it
      would become the most valuable square on the board.
    - We don't expect the strategy to be much of a winning one, but then again, 
      you never know.

