#ifndef __EXAMPLEPLAYER_H__
#define __EXAMPLEPLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
using namespace std;

class ExamplePlayer {

private:
    Board *board;
    Side mySide;
    Side opSide;
    
    int scoreBoard[64];
	void updateScoreBoard(Move *move);
	void deeperUpdate(Move *move);

	bool compromisable(int c);
	void updateBoard();
  	void update(int c);

public:
    ExamplePlayer(Side side);
    ~ExamplePlayer();
    
    
    Move *doMove(Move *opponentsMove, int msLeft);
	
};

#endif
